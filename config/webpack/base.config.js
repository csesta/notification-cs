const path = require('path');

module.exports = {
  entry: {
    app: './src/index.js'
  },
  output: {
    path: path.resolve('.', 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
    library: {
      root: 'notification-cs',
      commonjs: 'notification-cs'
    },
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  }
};
