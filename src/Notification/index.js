import classnames from 'classnames';

import classes from '../styles';

/**
 * Notification Class
 * The purpose of this class is to display notification messages.
 *
 * Class Variables
 * type (optional): NotificationType.SUCCESS, NotificationType.INFO, NotificationType.WARNING, or NotificationType.DANGER - defaults to NotificationType.INFO if not passed
 * title (optional): A String - defaults to null if not passed
 * subTitle (optional): A String - defaults to null of not passed
 * message (required): A String
 * timeout (optional): An Integer - defaults to 5000 if not passed - timeout value cannot be lower than 5000.
 * this.selfId: A Math.random() value
 */
function Notification(type, title, subTitle, message, timeout) {
  this.type = type;
  this.title = title;
  this.subTitle = subTitle;
  this.message = message;
  this.timeout = timeout;
  this.selfId = Math.random();
}

/**
 * Shift Notification Up
 * This function is used to stack all notifications on screen depending on how many exist.
 */
Notification.prototype.shiftNotification = function() {
  // TODO: function that shifts the position of multiple notifications depending on amount of notifications. Needs styles for added effect.
  if (document.getElementById('notification-list') !== null) {
    var padding = 16, // Padding default for bottom of the notifications
      notificationList = document.getElementById('notification-list').children,
      arrayLength = notificationList.length;

    notificationList[arrayLength - 1].clientHeight; // defaults the hight of the notification from -100px to 0px
    notificationList[arrayLength - 1].style.bottom = padding.toString() + 'px'; // add's a 16px padding to the bottom of the notification

    if (arrayLength > 1) {
      for (var i = arrayLength - 1; i > 0; i--) {
        var el = notificationList[i],
          prevEl = notificationList[i - 1];

        prevEl.style.bottom =
          (parseInt(el.style.bottom) + el.clientHeight + padding).toString() +
          'px';
      }
    }
  }
};

/**
 * Remove Notification
 * This function is used to remove a specific instance of notification from the screen.
 */
Notification.prototype.removeNotification = function() {
  var selfId = this.selfId;
  // TODO: function that removes the instance of the notification class. Needs styles for added effect.
  document.getElementById(selfId).className += ' fade-out';

  setTimeout(function() {
    document
      .getElementById('notification-list')
      .removeChild(document.getElementById(selfId));

    if (document.getElementById('notification-list').children.length === 0) {
      document
        .getElementsByTagName('body')[0]
        .removeChild(document.getElementById('notification-list'));
    }
  }, 1000);
};

/**
 * Show
 * This function is used to display the notification on the screen.
 */
Notification.prototype.show = function() {
  var self = this,
    callRemoveNotification = function() {
      self.removeNotification();
    };

  // if the navigation list container does not exist, create it.
  if (!document.getElementById('notification-list')) {
    var list = document.createElement('div');
    list.setAttribute('id', 'notification-list');
    document.getElementsByTagName('body')[0].appendChild(list);
  }

  var notification = document.createElement('div');
  notification.style.bottom = '-100px'; // Default hidden off screen for transition effect with fade-in
  notification.className = classnames(
    classes.notification,
    classes[this.type],
    classes.fadeIn
  );
  notification.setAttribute('id', this.selfId);

  if (this.title !== null) {
    var title = document.createElement('div');
    title.className = classes.title;
    title.appendChild(document.createTextNode(this.title));
    notification.appendChild(title);
  }

  if (this.subTitle !== null) {
    var subTitle = document.createElement('div');
    subTitle.className = classes.subTitle;
    subTitle.appendChild(document.createTextNode(this.subTitle));
    notification.appendChild(subTitle);
  }

  var message = document.createElement('div');
  message.className = classes.message;
  message.appendChild(document.createTextNode(this.message));

  notification.appendChild(message);
  document.getElementById('notification-list').appendChild(notification);
  this.shiftNotification();

  setTimeout(callRemoveNotification, this.timeout);
};

/**
 * Message
 * This static function instanciates an instance of the Notification class.
 * @param data
 */
Notification.message = function(data) {
  var timeoutDefault = 5000,
    timeout = data.timeout === undefined ? timeoutDefault : data.timeout;
  // Throw an error if no message has been defined.
  if (data.message === undefined) {
    throw new Error('A message needs to be provided.');
  }

  // Throw an error if specified timeout is below the default value
  if (timeout < timeoutDefault) {
    throw new Error(
      'The timeout value cannot be less than the default timeout of 5 seconds.'
    );
  }

  // creates instance of the notification class
  return new Notification(
    data.type === undefined ? NotificationType.INFO : data.type,
    data.title === undefined ? null : data.title,
    data.subTitle === undefined ? null : data.subTitle,
    data.message,
    timeout
  );
};

export default Notification;
