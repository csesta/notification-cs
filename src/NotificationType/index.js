/**
 * NotificationType Constants
 * These constants define the types of notifications that exist.
 * Based off css class names the determine the notifications color.
 *
 * CSS Class / Constant Relationship
 * SUCCESS: .notification-success - Green notification
 * INFO   : .notification-info - Blue notification
 * WARNING: .notification-warning - Orange notification
 * DANGER : .notification-danger - Red notification
 */
const NotificationType = {
  SUCCESS: 'success',
  INFO: 'info',
  WARNING: 'warning',
  DANGER: 'danger'
};

export default NotificationType;
