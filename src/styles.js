import jss from 'jss';
import preset from 'jss-preset-default';

// One time setup with default plugins and settings.
jss.setup(preset());

const styles = {
  notification: {
    padding: 16,
    color: '#000',
    position: 'fixed',
    minWidth: 100,
    maxWidth: 175,
    border: '1px solid transparent',
    borderRadius: 5,
    position: 'fixed',
    bottom: 0,
    right: 16,
    fontFamily: 'sans-serif',
    opacity: 0
  },
  title: {
    fontWeight: 600,
    fontSize: 14
  },
  subTitle: {
    fontWeight: 600,
    fontStyle: 'italic',
    fontSize: 13,
    paddingBottom: 2
  },
  message: {
    fontSize: 13
  },
  fadeIn: {
    transition: '1s',
    opacity: 1
  },
  fadeOut: {
    transition: '1s',
    right: 'calc(16px - 175px)',
    opacity: 0
  },
  success: {
    color: '#3c763d',
    backgroundColor: '#dff0d8',
    borderColor: '#d6e9c6'
  },
  info: {
    color: '#31708f',
    backgroundColor: '#d9edf7',
    borderColor: '#bce8f1'
  },
  warning: {
    color: '#8a6d3b',
    backgroundColor: '#fcf8e3',
    borderColor: '#faebcc'
  },
  danger: {
    color: '#a94442',
    backgroundColor: '#f2dede',
    borderColor: '#ebccd1'
  }
};

const { classes } = jss.createStyleSheet(styles).attach();

export default classes;
