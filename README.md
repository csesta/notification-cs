notification-cs is an easy to use pure javascript notification module.

## Object Properties
The following are a list of all properties that can be passed to the notification module:

* **type (optional)**: NotificationType.SUCCESS, NotificationType.INFO, NotificationType.WARNING, or NotificationType.DANGER - defaults to NotificationType.INFO if not passed
* **title (optional)**: A String - defaults to null if not passed
* **subTitle (optional)**: A String - defaults to null of not passed
* **message (required)**: A String
* **timeout (optional)**: An Integer - defaults to 5000 if not passed - timeout value cannot be lower than 5000.

## Notification Type's
The following are a list of existing notification types:

* **NotificationType.SUCCESS** - Green Notification
* **NotificationType.INFO** - Blue Notification
* **NotificationType.WARNING** - Orange Notification
* **NotificationType.DANGER** - Red Notification

## Example Call
The following is an example of how to call a notification.

```
import { Notification, NotificationType } = from 'notification-cs';

Notification.message({
    "type"    : NotificationType.INFO,
    "title"   : "Title",
    "subTitle": "Sub Title",
    "message" : "I am a random message"
}).show();

```
