import { Notification, NotificationType } from './dist/bundle';

function randomType() {
  var rand = Math.floor(Math.random() * 4);
  switch (rand) {
    case 0:
      return NotificationType.SUCCESS;
    case 1:
      return NotificationType.INFO;
    case 2:
      return NotificationType.WARNING;
    case 3:
      return NotificationType.DANGER;
  }
}

function randomMessage() {
  var rand = Math.floor(Math.random() * 4);
  switch (rand) {
    case 0:
      return 'I am a really really long message';
    case 1:
      return 'Error: Document ID has not been specified';
    case 2:
      return 'Successfully Created a User';
    case 3:
      return 'This account is not allowed. If you persist your account may be banned for good.';
  }
}

function submit() {
  Notification.message({
    type: randomType(),
    title: 'I am A Title',
    subTitle: 'I am a Sub Title',
    message: randomMessage()
  }).show();
}

const btn = document.getElementById('clickMe');

btn.onclick = () => {
  submit();
};
